from funding_opportunities_models.models.opportunity_topic import OpportunityTopic
from import_export import resources, fields


class OpportunityTopicResource(resources.ModelResource):

    def get_export_headers(self):
        headers = []
        for field in self.get_fields():
            model_fields = self.Meta.model._meta.get_fields()
            header = next((x.verbose_name for x in model_fields if x.name == field.column_name), field.column_name)
            headers.append(header)
        return headers

    class Meta:
        model = OpportunityTopic