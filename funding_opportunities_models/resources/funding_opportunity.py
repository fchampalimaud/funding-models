import json
from funding_opportunities_models.models.currency import Currency
from funding_opportunities_models.models.payment_frequency import PaymentFrequency
from funding_opportunities_models.models.opportunity_subject import OpportunitySubject
from funding_opportunities_models.models.grantor import Grantor
from funding_opportunities_models.models.funding_type import FundingType
from funding_opportunities_models.models.opportunity_topic import OpportunityTopic
from import_export import resources, fields
from import_export.widgets import ManyToManyWidget, ForeignKeyWidget
from funding_opportunities_models.models import FundingOpportunity, OpportunityDissemination


class DisseminationDatesWidget(ManyToManyWidget):

    def render(self, value, obj=None):
        # TODO: simplify field names from OpportunityDissemination
        result = list(value.values('opportunitydissemination_date', 'opportunitydissemination_email'))
        if result:
            return json.dumps(result, default=str)
        else:
            return None


class FundingOpportunityResource(resources.ModelResource):
    # TODO: rolling & published, change to true ou false
    fundingopportunity_source = fields.Field(attribute='get_fundingopportunity_source_display', column_name='Source')
    dissemination_dates = fields.Field(attribute='dissemination_dates', column_name='Dissemination dates', widget=DisseminationDatesWidget(OpportunityDissemination))

    currency = fields.Field(attribute='currency', column_name='Currency', widget=ForeignKeyWidget(Currency, 'currency_name'))
    paymentfrequency = fields.Field(attribute='paymentfrequency', column_name='Payment frequency', widget=ForeignKeyWidget(PaymentFrequency, 'paymentfrequency_name'))
    subject = fields.Field(attribute='subject', column_name='Subject', widget=ForeignKeyWidget(OpportunitySubject, 'opportunitysubject_name'))
    financingAgency = fields.Field(attribute='financingAgency', column_name='Grantor', widget=ForeignKeyWidget(Grantor, 'grantor_name'))
    fundingtype = fields.Field(attribute='fundingtype', column_name='Funding type', widget=ForeignKeyWidget(FundingType, 'fundingtype_name'))

    topics = fields.Field(attribute='topics', column_name='Topics', widget=ManyToManyWidget(OpportunityTopic, field='opportunitytopic_name'))

    def get_export_headers(self):
        headers = []
        for field in self.get_fields():
            model_fields = self.Meta.model._meta.get_fields()
            header = next((x.verbose_name for x in model_fields if x.name == field.column_name), field.column_name)
            headers.append(header)
        return headers

    class Meta:
        model = FundingOpportunity
        exclude = ( 'fundingopportunity_createdon',
                    'fundingopportunity_updatedon',
                    'fundingopportunity_brifdesc',
                    'fundingopportunity_desc',)

        export_order = ('fundingopportunity_id',
                        'fundingopportunity_published',
                        'financingAgency',
                        'fundingopportunity_name',
                        'fundingopportunity_end',
                        'subject',
                        'topics',
                        'fundingopportunity_link',
                        'fundingopportunity_source',
                        'fundingtype',
                        'fundingopportunity_value',
                        'currency',
                        'paymentfrequency',
                        'fundingopportunity_duration',
                        'fundingopportunity_eligibility',
                        'fundingopportunity_scope',
                        'fundingopportunity_rolling',
                        'fundingopportunity_loideadline',
                        'fundingopportunity_fullproposal',
                        'created',
                        'updated',)
