from django.db import models


class OpportunityDissemination(models.Model):

    opportunitydissemination_id = models.AutoField(primary_key=True)

    opportunitydissemination_date = models.DateField('Dissemination date')
    opportunitydissemination_email = models.EmailField('Dissemination destination email')
    
    class Meta:
        ordering = ['-opportunitydissemination_date']
        db_table = "fund_dissemination"

    def __unicode__(self): return str(self.opportunitydissemination_date)
    def __str__(self): return str(self.opportunitydissemination_date)
