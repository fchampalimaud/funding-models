from setuptools import setup, find_packages

setup(
    name="funding-models",
    version=0.1,
    description="""""",
    author=["Ricardo Ribeiro"],
    author_email=["ricardojvr@gmail.com"],
    packages=find_packages(),
)
